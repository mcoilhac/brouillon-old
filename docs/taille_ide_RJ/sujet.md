---
author: Romain Janvier
title: Essai de changement de la taille d'un IDE
---

??? question "Ton exo n'a pas besoin d'IDE plus grand"
    bla bla bla

    {{ IDE('test_Romain', SIZE=55) }}
    

??? question "Celui là en a besoin"
    bla bla bla

    {{ IDE('test', SIZE=55) }}


Suppression de l'essai sans admonition


