from timeit import timeit

fichier = open('pg798.txt', 'r', encoding = 'utf-8')
stendhal = fichier.read()
fichier.close()

def recherche_find(livre, texte):
    return livre.find(texte)

livre = stendhal
texte = 'Mme de Rênal fut fidèle à sa promesse'

temps_find = timeit("recherche_find(livre, texte)", number=10, globals=globals())
#temps_naif = timeit("recherche_naive(livre, texte)", number=10, globals=globals())
print("Temps en utilisant find : ",temps_find)
#print("Temps en utilisant l'algorithme naif : ",temps_naif)
