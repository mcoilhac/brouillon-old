---
author: Vincent Bouillot et Mireille Coilhac
title: Multi QCM
---




!!! info "Créer un QCM avec une autre présentation"

    ```markdown title="Le code à copier"
    Question 1 : Quelle est la réponse à la question universelle ?
    {% raw %}
    {{ qcm(["$6\\times 7$", "$\\int_0^{42} 1 dx$", "Je ne sais pas", "La réponse D"], [1,2], shuffle = True) }}
    {% endraw %}

    Question 2 : 1 + 1 = ?
    {% raw %}
    {{ qcm(["Je ne sais pas", "2", "L'âge du capitaine", "10 en binaire"], [2,4], shuffle = True) }}
    {% endraw %}
    ```

    <div class="result" markdown>

    Question 1 : Quelle est la réponse à la question universelle ?
    {{ qcm(["$6\\times 7$", "$\\int_0^{42} 1 dx$", "`#!python sum([i for i in range(10)])`", "La réponse D"], [1,2], shuffle = True) }}
    
    Question 2 : 1 + 1 = ?
    {{ qcm(["Je ne sais pas", "2", "L'âge du capitaine", "10 en binaire"], [2,4], shuffle = True) }}
    
    </div>

Question avec Python : Quelle est la réponse à la question universelle ?

{{ qcm(["$6\\times 7$", "$\\int_0^{42} 1 dx$", "`#!python sum([i for i in range(10)])`", "La réponse D"], [1,2], shuffle = True) }}
    


## Multi QCM



{{multi_qcm(
  ["1+1=?", ["$6\\times 7$", "Ça : $\\int_0^{42} 1 \\textrm{d} x$", "`#!python sum([i for i in range(10)])`", "La réponse D", '42'], [1,2, 5]],
  ["1+1=?", ["$12$", "2", "Je sais pas", "L'age du capitaine"], [2]],
  ["$x - {n} = {p}$", ["$x = {n} + {p}$", "$x = {n} - {p}$", "$x = {p}$", "$x = {n} / {p}$"], [1], {'p' : [1, 3, 7], 'n' : [4, 2, 1]} ],  
  ["Résoudre l'équation $x - {n} = {p}$ pour $x>{p}$.", ["$x = |{n} + {p}|$", "$x = |{n} - {p}|$", "$x = |{p}|$", "$x = \\dfrac{{n}}{{p}}$"], [1], {'p' : [1, 3, 7], 'n' : [4, 2, 1]} ]
)}}

!!! info "QCM à volets"

    === "QCM"

        Cliquez sur vos propositions aux questions puis validez.

        {{multi_qcm(
        ["Quelle est la réponse à la question universelle ? Cocher deux réponses.", 
        ["$6\\times 7$", "$\\int_0^{42} 1 dx$", "Je ne sais pas", "La réponse D"], [1,2]],
        ["1 + 1 = ? Cocher deux réponses.",
        ["Je ne sais pas", "2", "L'âge du capitaine", "10 en binaire"], [2,4]]
        )}}


    === "Réponses"

        Cliquez sur les différentes propositions


        Question 1 : Quelle est la réponse à la question universelle ? Cocher deux réponses.
        {{ qcm(["$6\\times 7$", "$\\int_0^{42} 1 dx$", "Je ne sais pas", "La réponse D"], [1,2], shuffle = True) }}

        Question 2 : 1 + 1 = ? Cocher deux réponses.
        {{ qcm(["Je ne sais pas", "2", "L'âge du capitaine", "10 en binaire"], [2,4], shuffle = True) }}


    === "Commentaires"

        Lisez bien les remarques suivantes


        **Question 1 :** Commentaire 1

        **Question 2 :** Commentaire 1


???+ question "Mon exercice"

    Cliquez sur vos propositions aux questions puis validez.

    {{multi_qcm(
    ["Comment saisir un entier `a` ? Cocher une réponse.", 
    ["`a = int(input())`", "`a = input()`", "Je ne sais pas", "La réponse D"], [1]],
    ["Qu'affiche `print('3' + '3')` ?",
    ["Je ne sais pas", 6, "L'âge du capitaine", 33], [4]]
    )}}

???+ question "Le même avec coloration Python"

    Cliquez sur vos propositions aux questions puis validez.

    {{multi_qcm(
    ["Comment saisir un entier `#!py a` ? Cocher une réponse.", 
    ["`a = int(input())`", "`#!py a = input()`", "Je ne sais pas", "La réponse D"], [1]],
    ["Qu'affiche `#!py print('3' + '3')` ?",
    ["Je ne sais pas", " `#!py 6`", "L'âge du capitaine", " `#!py 33`"], [4]]
    )}}

???+ question "Mon exercice sans espace rajouté"

    Que s'affiche-t-il ? Cliquez sur vos propositions aux questions puis validez.

    {{multi_qcm(
    ["`#!py print('hello')`", 
    ["`#!py a = int(input())`", "`#!py a = input()`", "Je ne sais pas", "La réponse D"], [1]],
    ["Qu'affiche `print('3' + '3')` ?",
    ["Je ne sais pas", "#!py 6", "L'âge du capitaine", "#!py 33"], [4]]
    )}}

Sans admonition ni espace rajouté

{{multi_qcm(
["`#!py print('hello')`", 
["`a = int(input())`", "`a = input()`", "Je ne sais pas", "La réponse D"], [1]],
["Qu'affiche `print('3' + '3')` ?",
["Je ne sais pas", 6, "L'âge du capitaine", 33], [4]]
)}}

hors admonition question latex

{{multi_qcm(
  ["$\\int_0^{42} 1 \\textrm{d} x$ = ?", ["$6\\times 7$", "Ça : $\\int_0^{42} 1 \\textrm{d} x$", "`#!py sum([i for i in range(10)])`", "La réponse D", '42'], [1,2, 5]],
  ["1+1=?", ["$12$", "2", "Je sais pas", "L'age du capitaine"], [2]],
  ["$x - {n} = {p}$", ["$x = {n} + {p}$", "$x = {n} - {p}$", "$x = {p}$", "$x = {n} / {p}$"], [1], {'p' : [1, 3, 7], 'n' : [4, 2, 1]} ],  
  ["Résoudre l'équation $x - {n} = {p}$ pour $x>{p}$.", ["$x = |{n} + {p}|$", "$x = |{n} - {p}|$", "$x = |{p}|$", "$x = \\dfrac{{n}}{{p}}$"], [1], {'p' : [1, 3, 7], 'n' : [4, 2, 1]} ]
)}}

Autre essai hors admonition en supprimant l'espace ajouté 

{{multi_qcm(
  ["`#!py sum([i for i in range(10)])`", ["$6\\times 7$", "Ça : $\\int_0^{42} 1 \\textrm{d} x$", "`#!py sum([i for i in range(10)])`", "La réponse D", '42'], [1,2, 5]],
  ["1+1=?", ["$12$", "2", "Je sais pas", "L'age du capitaine"], [2]],
  ["$x - {n} = {p}$", ["$x = {n} + {p}$", "$x = {n} - {p}$", "$x = {p}$", "$x = {n} / {p}$"], [1], {'p' : [1, 3, 7], 'n' : [4, 2, 1]} ],  
  ["Résoudre l'équation $x - {n} = {p}$ pour $x>{p}$.", ["$x = |{n} + {p}|$", "$x = |{n} - {p}|$", "$x = |{p}|$", "$x = \\dfrac{{n}}{{p}}$"], [1], {'p' : [1, 3, 7], 'n' : [4, 2, 1]} ]
)}}


Autre essai 3 hors admonition avec suppression espace

{{multi_qcm(
  ["`sum([i for i in range(10)])`", ["$6\\times 7$", "Ça : $\\int_0^{42} 1 \\textrm{d} x$", "`#!python sum([i for i in range(10)])`", "La réponse D", '42'], [1,2, 5]],
  ["1+1=?", ["$12$", "2", "Je sais pas", "L'age du capitaine"], [2]],
  ["$x - {n} = {p}$", ["$x = {n} + {p}$", "$x = {n} - {p}$", "$x = {p}$", "$x = {n} / {p}$"], [1], {'p' : [1, 3, 7], 'n' : [4, 2, 1]} ],  
  ["Résoudre l'équation $x - {n} = {p}$ pour $x>{p}$.", ["$x = |{n} + {p}|$", "$x = |{n} - {p}|$", "$x = |{p}|$", "$x = \\dfrac{{n}}{{p}}$"], [1], {'p' : [1, 3, 7], 'n' : [4, 2, 1]} ]
)}}


???+ question "Provisoire 1 "

    Question 1 : Qu'affiche `#!python print("Hello")`

    Question 2 : Qu'affiche `#!py print("1" + "1")`

    Question 3 : Comment saisir une variable `a` de type `int`

    Question 4 : Comment saisir une variable `a` de type `float`

    {{multi_qcm(
      [" ", ["$6\\times 7$", "Ça : $\\int_0^{42} 1 \\textrm{d} x$", "`#!python sum([i for i in range(10)])`", "La réponse D", '42'], [1,2, 5]],
      [" ", ["$12$", "2", "Je sais pas", "L'age du capitaine"], [2]],
      [" ", ["$x = {n} + {p}$", "$x = {n} - {p}$", "$x = {p}$", "$x = {n} / {p}$"], [1], {'p' : [1, 3, 7], 'n' : [4, 2, 1]} ],  
      [" ", ["$x = |{n} + {p}|$", "$x = |{n} - {p}|$", "$x = |{p}|$", "$x = \\dfrac{{n}}{{p}}$"], [1], {'p' : [1, 3, 7], 'n' : [4, 2, 1]} ]
    )}}

???+ question "Provisoire 2 "

    === "Question 1"

        Qu'affiche `#!python print("Hello")`

    === "Question 2" 
    
        Qu'affiche `#!py print("1" + "1")`

    === "Question 3"
    
        Comment saisir une variable `a` de type `int`

    === "Question 4"
    
        Comment saisir une variable `a` de type `float`

    {{multi_qcm(
      [" ", ["$6\\times 7$", "Ça : $\\int_0^{42} 1 \\textrm{d} x$", "`#!python sum([i for i in range(10)])`", "La réponse D", '42'], [1,2, 5]],
      [" ", ["$12$", "2", "Je sais pas", "L'age du capitaine"], [2]],
      [" ", ["$x = {n} + {p}$", "$x = {n} - {p}$", "$x = {p}$", "$x = {n} / {p}$"], [1], {'p' : [1, 3, 7], 'n' : [4, 2, 1]} ],  
      [" ", ["$x = |{n} + {p}|$", "$x = |{n} - {p}|$", "$x = |{p}|$", "$x = \\dfrac{{n}}{{p}}$"], [1], {'p' : [1, 3, 7], 'n' : [4, 2, 1]} ]
    )}}


???+ question "Les tableaux - QCM 1"

    Cocher toutes les affirmations correctes pour chaque question.

    === "Question 1"

        On considère le tableau `#!py meubles = ['Table', 'Commode', 'Armoire', 'Placard', 'Buffet']`.


    === "Question 2"

        On considère le tableau `#!py pointures = [38, 43, 44, 43, 37, 42, 39, 43, 40]`.


    === "Question 3"

        Par quoi faut-il complter le script ci-dessous pour que s'affichent tous les éléments de la liste 
        
        `#!py meubles = ['Table', 'Commode', 'Armoire', 'Placard', 'Buffet']` ?

        ```python
        for indice in range(...):
            print(meubles[indice])
        ```

    {{multi_qcm(
      [" ", [" `#!py meubles[1]` vaut `#!py 'Table'`", " `#!py meubles[1]` vaut `#!py 'Commode'`", " `#!py meubles[4]` vaut `#!py 'Buffet'`"," `#!py meubles[5]` vaut `#!py 'Buffet'`"], [2, 3]],
      [" ", ["Ce tableau est mal défini car il contient des valeurs en double", " `#!py pointures[38]`  vaut  0", " `#!py pointures[3]`  est égal à `pointures[8]`", " `#!py pointures[len(pointures) - 1]`  vaut 40"], [4]],
      [" ", ["4", "5", "6"], [2]]
    )}}


???+ question "Les tableaux - QCM 2"

    Cocher toutes les affirmations correctes pour chaque question.

    === "Question 1"

        On considère le tableau `#!py meubles = ['Table', 'Commode', 'Armoire', 'Placard', 'Buffet']`.


    === "Question 2"

        On considère le tableau `#!py pointures = [38, 43, 44, 43, 37, 42, 39, 43, 40]`.


    === "Question 3"

        Par quoi faut-il complter le script ci-dessous pour que s'affichent tous les éléments de la liste 
        
        `#!py meubles = ['Table', 'Commode', 'Armoire', 'Placard', 'Buffet']` ?

        ```python
        for indice in range(...):
            print(meubles[indice])
        ```

    {{multi_qcm(
      [" ", [" `#!py meubles[1]` vaut `#!py 'Table'`", " `#!py meubles[1]` vaut `#!py 'Commode'`", " `#!py meubles[4]` vaut `#!py 'Buffet'`"," `#!py meubles[5]` vaut `#!py 'Buffet'`"], [2, 3]],
      [" ", ["Ce tableau est mal défini car il contient des valeurs en double", " `#!py pointures[38]`  vaut  `0`", " `#!py pointures[3]`  est égal à `pointures[8]`", " `#!py pointures[len(pointures) - 1]`  vaut `40`"], [4]],
      [" ", ["4", "5", "6"], [2]]
    )}}


???+ question "QCM"

    Cocher toutes les affirmations correctes pour chaque question.

    === "Question 1"

        Quel message affiche l’ordinateur lorsque l'utilisateur saisit 8.5 ?

		```python linenums='1'
        nombre = int (input("Saisir un nombre") )
        double = nombre * 2
        print(double)
		```


    === "Question 2"

        On a exécuté le code suivant :

		```python linenums='1'
		nombre_d_invites = 25
        nombre_de_courriers = 11
        print(nombre_de_courrier)
		```
        On obtient le message suivant : 

        `Traceback (most recent call last):`  
        `File "<module1>", line 3, in <module>`  
        `NameError: name 'nombre_de_courrier' is not defined`

        Que signifie ce message d'erreur ?


    === "Question 3"

        On a exécuté le code suivant :

		```python linenums='1'
        cote = input("quel est la longueur du coté du carré ?")
        aire = cote ** 2
        print(aire)
		```
        On obtient le message suivant : 

        `Traceback (most recent call last):`  
        `File "<module1>", line 2, in <module>`  
        `TypeError: unsupported operand type(s) for ** or pow(): 'str' and 'int'`

        Que signifie ce message d'erreur ?

	=== "Question 4"

        On a exécuté le code suivant :

		```python linenums='1'
        cote = 5
          aire = cote ** 2
        print(aire)
		```

        Quel type de message d'erreur va-t-on obtenir ?  


    === "Question 5"

        Quel message affiche l’ordinateur lorsque l'utilisateur saisit 5 ?

		```python linenums='1'
        nombre = input("Saisir un nombre")
        triple = nombre * 3
        print(triple)
		```

    {{multi_qcm(
      [" ", ["L'ordinateur affiche une erreur", "17", "16.0", 16], [1]],
      [" ", ["On a fait une erreur de syntaxe", "On effectue une opération incorrecte entre deux valeurs de types différents", "Problème d'indentation du code"
      , "On utilise une variable non définie"], [4]],
      [" ", ["On effectue une opération impossible entre deux valeurs de types différents", "On a mal indenté le code", "On a utilisé une variable qui n'est pas définie","On a fait une erreur de syntaxe"], [1]],
      [" ", ["TypeError ", "NameError", "SyntaxError", "IndentError"], [4]],
      [" ", ["15", "15.0", "555", "nombrenombrenombre"], [3]],
    )}}


## Essai pour DS

Question 1 : Quel est le type de  `ma_variable` dans le code suivant : `ma_variable = "45 < 50"`

{{ qcm(["str", "int", "float", "bool", "autre"], [1], shuffle = True) }}


Question 2 : Quel est le type de  `ma_variable` dans le code suivant : `ma_variable = 45 < 50`

{{ qcm(["str", "int", "float", "bool", "autre"], [4], shuffle = True) }}


Question 3 : Quel est le type de  `ma_variable` dans le code suivant : `ma_variable = input("Saisir votre donéée")`

{{ qcm(["str", "int", "float", "bool", "autre"], [1], shuffle = True) }}


Question 4 : On exécute le code ci-dessous. Que contient la variable `a` à la fin de l’exécution de ce script ? 

```python
a = 76
if a < 5:
    a = 10
elif a < 100:
    a = 200
elif a < 1000:
    a = 2000
else:
    a = 0
```

{{ qcm(["0", "10", "200", "2000", "autre"], [3], shuffle = True) }}


Question 5 : On exécute le code ci-dessous. Que contient la variable `reponse` à la fin de l’exécution de ce script ? 


```python
x = 5
y = 5
z = 10
reponse = x**2 + y**2 == z**2
```

{{ qcm(["50", "False", "True", "100", "autre"], [2], shuffle = True) }}


Question 6 : On exécute le code ci-dessous. ipt ? Quelles expressions booléennes prennent la valeur `True` ? Cocher **toutes** les réponses possibles

```python
a = 5
b = 10
```

{{ qcm(["`#!python a!=5 or b == 8`", "`#!python b < 11`", "`#!python a == 5 or b != 8`", "`#!python a == 5 and b != 8`"], [2, 3, 4], shuffle = True) }}


Question 7 : Quelles sont les valeurs possibles des variables booléennes `a` et `b` pour que l'expression booléenne : `not(a or b)` soit évaluée à `True` ?

{{ qcm(["`#!python a = False, b = False`", "`#!python a = False, b = True`", "`#!python a = True, b = False`", "`#!python a = True, b = True`", "autre"], [1], shuffle = True) }}


Question 8 : A quelle expression booléenne correspond la table de vérité suivante? 

|a|b|?|
|:---:|:---:|:---:|
|0|0|1|
|0|1|1|
|1|0|1|
|1|1|0|

{{ qcm(["a ou b", "non(a ou b)", "a et b", "non(a et b)", "Autre"], [4], shuffle = True) }}


Question 9 : A quelle expression booléenne correspond la table de vérité suivante? 

|a|b|?|
|:---:|:---:|:---:|
|0|0|0|
|0|1|1|
|1|0|1|
|1|1|1|

{{ qcm(["a ou b", "non(a ou b)", "a et b", "non(a et b)", "autre"], [1], shuffle = True) }}


Question 10 : Qu'affiche le code ci-dessous ? 

```python
x = True
y = False
z = False
  
if (x or y) and z: 
    print("Hello")
else: 
    print("World")
```

{{ qcm(["Hello", "World", "hello World", "Une erreur", "Autre"], [2], shuffle = True) }}


Question 11 : On exécute le code ci-dessous. Que contient la variable `ma_variable` à la fin de l’exécution de ce script ? 

```python
ma_variable = 7
ma_variable = ma_variable + ma_variable 
ma_variable = ma_variable + ma_variable
```

{{ qcm(["77777", "14", "35", "28", "Autre"], [4], shuffle = True) }}


Question 12 : Que faut-il écrire à la place de `...` pour que la valeur affichée soit 5 ? 

```python
s = 0
for i in range(...):
    s = s + 1
print(s)
```

{{ qcm(["4", "5", "6", "Autre"], [2], shuffle = True) }}


Question 13 : On a `a = True` et `b = False`. que vaut l'expression booléenne `(b and a) or (not a or b)` ? 

{{ qcm(["True et False en même temps", "ni True ni False", "False", "True", "autre"], [3], shuffle = True) }}


Question 14 : Les variables  a et b sont booléennes. L'expression booléenne (a and b) and not a est équivalente à

{{ qcm(["False", "True", "not b", "not a or not b", "Autre"], [1], shuffle = True) }}


Question 15 : On exécute le code ci-dessous. Cocher toutes les propositions qui s'évaluent à True parmi les propositions suivantes.

```python
x = 6
y = 7
```

{{ qcm(["`#!python x != 6 or y == 9`", "`#!python y < 8`", "`#!python x == 6 or y == 8`", "`#!python x == 6 and y == 5`", "Aucune"], [2, 3], shuffle = True) }}


Question 16 : On exécute le code ci-dessous. Que s'affiche-t-il ?
```python
x = 3
y = 8
x = y
y = x
print(x, y)
```

{{ qcm(["8, 3", "3, 3", "8, 8", "8, 3"], [3], shuffle = True) }}


Question 17 : On exécute le code ci-dessous. Que s'affiche-t-il ?
```python
a = 9
if a < 5:
   a = 20
elif a < 98:
   a = 697
elif a < 1000:
   a = 1
else:
   a = 0
print(a)
```

{{ qcm(["0", "1", "20", "697", "Autre"], [4], shuffle = True) }}


Question 18 : On exécute le code ci-dessous. Que s'affiche-t-il ?
```python
variable_1 = "8"
variable_2 = "7"
print(variable_1 + variable_2)
```

{{ qcm(["15", "87", "Il va s'afficher une erreur", "42", "Autre"], [2], shuffle = True) }}


Question 19 : Cocher tous les codes pour leqquelq la variable `i` prend successivement les valeurs 0, 1, 2, 3, 4 ?

{{ qcm(["`#!python for i in range(5)`", "`#!python for i in range(4)`", "`#!python for i in range(0, 4, 1)`", "`#!python for i in range(0, 5, 1)`"], [1, 4], shuffle = True) }}


Question 20 : Que faut-il écrire à la place de `...` pour qu'il s'affiche le résultat de 2 + 3 + 4 + 5 ?

```python
somme = 0
for i in range(...):
   somme = somme + i
print(somme)
```

{{ qcm(["2, 1, 5", "2, 5", "2, 6", "1, 5", "Autre"], [3], shuffle = True) }}