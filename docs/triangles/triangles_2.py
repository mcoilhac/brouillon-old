from turtle import *

def motif(cote, n) :
    if n == 1 :
        for i in range(3) :
            forward(cote)
            left(120)
    else :
        for i in range(3) :
            motif(cote/2, n - 1)
            forward(cote)
            left(120)

#hideturtle()  # on cache la tortue
#speed(0) # tortue rapide
cote = 200
motif(cote, 3)
done()

