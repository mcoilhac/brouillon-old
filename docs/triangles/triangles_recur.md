---
author: Mireille Coilhac
title: Triangles récursifs
tags:
  - 6-récursivité
---


## Fractales



???+ question "Exercice 1"
    <div class="centre" markdown="span">
    <iframe 
    src="https://console.basthon.fr/?from={{ page.canonical_url }}../scripts/triangles.py"
    width="900" height="500" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>


???+ question "Exercice 2"
    <div class="centre" markdown="span">
    <iframe 
    src="https://console.basthon.fr/?from={{ page.canonical_url }}../triangles_2.py"
    width="900" height="500" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

???+ question "Exercice 3"
    <div class="centre" markdown="span">
    <iframe 
    src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../scripts/les_triangles.ipynb"
    width="900" height="500" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>