---
author: Mireille Coilhac
title: QCM Première 2 le 3 octobre 2023
---

**Nom :**



Question 1 : Quel est le type de  `ma_variable` dans le code suivant : `ma_variable = "45 < 50"`

{{ qcm(["str", "int", "float", "bool", "autre"], [1], shuffle = True) }}


Question 2 : Quel est le type de  `ma_variable` dans le code suivant : `ma_variable = 45 < 50`

{{ qcm(["str", "int", "float", "bool", "autre"], [4], shuffle = True) }}


Question 3 : Quel est le type de  `ma_variable` dans le code suivant : `ma_variable = input("Saisir votre donnée")`

{{ qcm(["str", "int", "float", "bool", "autre"], [1], shuffle = True) }}


Question 4 : On exécute le code ci-dessous. Que contient la variable `a` à la fin de l’exécution de ce code ? 

```python
a = 76
if a < 5:
    a = 10
elif a < 100:
    a = 200
elif a < 1000:
    a = 2000
else:
    a = 0
```

{{ qcm(["0", "10", "200", "2000", "autre"], [3], shuffle = True) }}


Question 5 : On exécute le code ci-dessous. Que contient la variable `reponse` à la fin de l’exécution de ce code ? 


```python
x = 5
y = 5
z = 10
reponse = x**2 + y**2 == z**2
```

{{ qcm(["50", "False", "True", "100", "autre"], [2], shuffle = True) }}


Question 6 : On exécute le code ci-dessous. Quelles expressions booléennes prennent la valeur `True` ? Cocher **toutes** les réponses possibles

```python
a = 5
b = 10
```

{{ qcm(["`#!python a!=5 or b == 8`", "`#!python b < 11`", "`#!python a == 5 or b != 8`", "`#!python a == 5 and b != 8`"], [2, 3, 4], shuffle = True) }}


Question 7 : Quelles sont les valeurs possibles des variables booléennes `a` et `b` pour que l'expression booléenne : `not(a or b)` soit évaluée à `True` ?

{{ qcm(["`#!python a = False, b = False`", "`#!python a = False, b = True`", "`#!python a = True, b = False`", "`#!python a = True, b = True`", "autre"], [1], shuffle = True) }}


Question 8 : À quelle expression booléenne correspond la table de vérité suivante ? 

|a|b|?|
|:---:|:---:|:---:|
|0|0|1|
|0|1|1|
|1|0|1|
|1|1|0|

{{ qcm(["a ou b", "non(a ou b)", "a et b", "non(a et b)", "Autre"], [4], shuffle = True) }}


Question 9 : À quelle expression booléenne correspond la table de vérité suivante ? 

|a|b|?|
|:---:|:---:|:---:|
|0|0|0|
|0|1|1|
|1|0|1|
|1|1|1|

{{ qcm(["a ou b", "non(a ou b)", "a et b", "non(a et b)", "autre"], [1], shuffle = True) }}


Question 10 : Qu'affiche le code ci-dessous ? 

```python
x = True
y = False
z = False
  
if (x or y) and z: 
    print("Hello")
else: 
    print("World")
```

{{ qcm(["Hello", "World", "Hello World", "Une erreur", "Autre"], [2], shuffle = True) }}


Question 11 : On exécute le code ci-dessous. Que contient la variable `ma_variable` à la fin de l’exécution de ce code ? 

```python
ma_variable = 7
ma_variable = ma_variable + ma_variable 
ma_variable = ma_variable + ma_variable
```

{{ qcm(["77777", "14", "35", "28", "Autre"], [4], shuffle = True) }}


Question 12 : Que faut-il écrire à la place de `...` pour que la valeur affichée soit 5 ? 

```python
s = 0
for i in range(...):
    s = s + 1
print(s)
```

{{ qcm(["4", "5", "6", "Autre"], [2], shuffle = True) }}


Question 13 : On a `a = True` et `b = False`. que vaut l'expression booléenne `(b and a) or (not a or b)` ? 

{{ qcm(["True et False en même temps", "ni True ni False", "False", "True", "autre"], [3], shuffle = True) }}


Question 14 : Les variables  a et b sont booléennes. L'expression booléenne (a and b) and not a est équivalente à

{{ qcm(["False", "True", "not b", "not a or not b", "Autre"], [1], shuffle = True) }}


Question 15 : On exécute le code ci-dessous. Cocher toutes les propositions qui s'évaluent à True parmi les propositions suivantes.

```python
x = 6
y = 7
```

{{ qcm(["`#!python x != 8 or y == 9`", "`#!python y < 8`", "`#!python x == 6 or y == 8`", "`#!python x == 6 and y == 5`", "Aucune"], [1, 2, 3], shuffle = True) }}


Question 16 : On exécute le code ci-dessous. Que s'affiche-t-il ?
```python
x = 3
y = 8
x = y
y = x
print(x, y)
```

{{ qcm(["8, 3", "3, 3", "8, 8", "8, 3"], [3], shuffle = True) }}


Question 17 : On exécute le code ci-dessous. Que s'affiche-t-il ?
```python
a = 9
if a < 5:
   a = 20
elif a < 98:
   a = 697
elif a < 1000:
   a = 1
else:
   a = 0
print(a)
```

{{ qcm(["0", "1", "20", "697", "Autre"], [4], shuffle = True) }}


Question 18 : On exécute le code ci-dessous. Que s'affiche-t-il ?
```python
variable_1 = "8"
variable_2 = "7"
print(variable_1 + variable_2)
```

{{ qcm(["15", "87", "Il va s'afficher une erreur", "42", "Autre"], [2], shuffle = True) }}


Question 19 : Cocher tous les codes pour lesquels la variable `i` prend successivement les valeurs 0, 1, 2, 3, 4 

{{ qcm(["`#!python for i in range(5)`", "`#!python for i in range(4)`", "`#!python for i in range(0, 4, 1)`", "`#!python for i in range(0, 5, 1)`"], [1, 4], shuffle = True) }}


Question 20 : Que faut-il écrire à la place de `...` pour qu'il s'affiche le résultat de 2 + 3 + 4 + 5.

```python
somme = 0
for i in range(...):
   somme = somme + i
print(somme)
```

{{ qcm(["2, 1, 5", "2, 5", "2, 6", "1, 5", "Autre"], [3], shuffle = True) }}