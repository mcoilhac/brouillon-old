---
author: Mireille Coilhac
title: QCM Sécurisation des communications
---

{{ multi_qcm(
  ["Quel est la principale limite du chiffrement symétrique ?", ["La sécurité incertaine lors du transfert de la clé", "La lenteur des opérations de chiffrement et de déchiffrement", "Le chiffrement nécessite un nombre très important de calculs", "Des clés différentes sont utilisées pour chiffrer et déchiffrer"], [1]],
  ["Dans le cas d'un chiffrement asymétrique, à quoi sert la clé secrète ?", ["Déchiffrer illégitimement le message", "Chiffrer le message", "Dissimuler le message dans un support", "Déchiffrer afin de retrouver le message original"], [4]],
  ["Bob veut envoyer un message privé à Alice. Qu'est-ce qui est vrai ?", ["Alice a besoin de la clé privée de Bob", "Alice a besoin de la clé publique de Bob", "Bob a besoin de la clé privée d'Alice", "Bob a besoin de la clé publique d'Alice"], [4]],  
  ["Un chiffrement asymétrique est vulnérable à : ", ["L'attaque par force brute", "L'attaque dite de l'homme du milieu", "L'attaque par dictionnaire", "L'attaque par indice"], [2]],
  ["Cocher toutes les bonnes réponses", ["Dans un chiffrement asymétrique, deux clés sont partagées", "Dans un chiffrement symétrique, une seule clé est partagée", "Une clé de chiffrement doit toujours rester secrète", "Le chiffrement RSA est symétrique"], [2]]
)}}