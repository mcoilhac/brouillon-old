# tests

assert nb_sommes(3) == 3
assert nb_sommes(5) == 7


# autres tests

assert nb_sommes(1) == 1
assert nb_sommes(2) == 2
assert nb_sommes(4) == 5
assert nb_sommes(10) == 42
# assert nb_sommes(42) == 53174

def OFF_nb_sommes(n):
    def OFF_nb_k_sommes(n, k):
        if n < k:
            return 0
        elif k == 1:
            return 1
        else:
            return OFF_nb_k_sommes(n - 1, k - 1) + OFF_nb_k_sommes(n - k, k)
    
    return sum(OFF_nb_k_sommes(n, k) for k in range(1, 1 + n))

for n in range(1, 10):
    attendu = OFF_nb_sommes(n)
    assert nb_sommes(n) == attendu, f"Erreur avec n = {n}"
