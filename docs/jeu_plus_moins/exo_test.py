# Tests secrets

class Joueur:
    def __init__(self, secret=None, reponse=None):
        self.debut = 1
        self.fin = 99
        self.compteur = 0
        self.secret = secret
        self.reponse = reponse  # on va toujours proposer cette réponse si pas None
        if self.reponse is not None:
            self.milieu = self.reponse
        else:
            self.milieu = (self.debut + self.fin) // 2

    def input(self, texte):
        if "petit" in texte:
            if self.reponse is None:
                self.debut = self.milieu + 1
            if self.secret is not None:
                assert self.secret >= self.milieu, "menteur"
        elif "grand" in texte:
            if self.reponse is None:
                self.fin = self.milieu - 1
            if self.secret is not None:
                assert self.secret <= self.milieu, "menteur"
        if self.reponse is None:
            self.milieu = (self.debut + self.fin) // 2
        self.compteur += 1
        return self.milieu

    def print(self, *args, **kwargs):
        assert len(args) == 2, f"print n'est pas appelé comme prévu {args}"
        texte, valeur = args
        if "Bravo" in texte:
            assert valeur == self.milieu, "mauvais nombre affiché"
            assert valeur == randint(1, 99), "pas le bon nombre secret"
        elif "Perdu" in texte:
            assert self.compteur == 10, "la partie se termine trop tôt"
            assert valeur != self.milieu, "pourtant la valeur avait été trouvée"
            assert valeur == randint(1, 99), "pas le bon nombre secret"
            assert self.debut <= valeur <= self.fin, "les réponses ne sont pas cohérentes"
        else:
            assert self.compteur == valeur, "le nombre d'essais n'est pas le bon"

# On va tester toutes les valeurs
vrai_print = print
for i in range(1, 100):
    # On surcharge randint pour choisir le résultat
    randint = lambda x, y: i
    joueur = Joueur()  # joueur normal
    input = joueur.input
    print = joueur.print
    joueur = Joueur(secret=i)  # joueur qui connaît le secret
    input = joueur.input
    print = joueur.print
    plus_ou_moins()
    joueur = Joueur(secret=i, reponse=i)  # joueur qui trouve tout de suite
    input = joueur.input
    print = joueur.print
    plus_ou_moins()
    if i < 99:
        joueur = Joueur(secret=i, reponse=i+1)  # joueur qui dit toujours trop grand
        input = joueur.input
        print = joueur.print
        plus_ou_moins()
    if i > 1:
        joueur = Joueur(secret=i, reponse=i-1)  # joueur qui dit toujours trop petit
        input = joueur.input
        print = joueur.print
        plus_ou_moins()

print = vrai_print  # on remet le print normal
