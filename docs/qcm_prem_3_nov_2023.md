---
author: Mireille Coilhac
title: QCM Première 3 le 29 novembre 2023
---

**Nom :**

**⚠️ Une bonne réponse rapporte 1 point, une mauvaise réponse enlève 0.25 point, l'absence de réponse ne rapporte aucun point ni n'enlève aucun point.**

Question 1 : Qu'affiche le code ci-dessous ? 

```python
ma_variable = 45 == 45
print(ma_variable)
```

{{ qcm(["un message d'erreur", "`#!python 45 == 45`", "`#!python True`", "45", "Autre"], [3], shuffle = True) }}

Question 2 : À quelle expression booléenne correspond la table de vérité suivante ? 

|a|b|?|
|:---:|:---:|:---:|
|0|0|0|
|0|1|1|
|1|0|1|
|1|1|1|

{{ qcm(["a ou b", "non(a ou b)", "a et b", "non(a et b)", "Autre"], [4], shuffle = True) }}


Question 3 : 152 est écrit en décimal. Le convertir en binaire sur un octet.

{{ qcm(["0111 1101", "1101 1001", "1001 1000", "1001 0110", "Autre"], [3], shuffle = True) }}


Question 4 : Donner le résultat de l'addition binaire 101111 + 001010

{{ qcm(["111001", "111111", "111001", "101111", "Autre"], [3], shuffle = True) }}


Question 5 : En logique l'expression `non (a et b)` est équivalente à

{{ qcm(["`#!python (non a) ou (non b))`", "`#!python (non a) et (non b))`", "`#!python a et b`", "`#!python a ou b`", "Autre"], [1], shuffle = True) }}


Question 6 : 0011 1001 est écrit en binaire sur un octet. Le convertir en décimal.

{{ qcm(["25", "59", "93", "57", "92", "25", "Autre"], [4], shuffle = True) }}


Question 7 : Que contient la variable `n` après l'exécution du code suivant ?

```python
n = 3
while n < 10:
    n = n * 2
```

{{ qcm(["6", "12", "24", "48", "Autre"], [2], shuffle = True) }}


Question 8 : Que contient la variable `n` après l'exécution du code suivant ?

```python
n = 0
for i in range(1, 3):
    n = n + i
```

{{ qcm(["6", "10", "4", "3", "Autre"], [4], shuffle = True) }}


Question 9 : Que s'affiche-t-il ?

```python
pays = ["France", "Allemagne", "Italie", "Belgique"]
print(pays[1])
```

{{ qcm(["France", "Allemagne", "Italie", "Belgique"], [2], shuffle = True) }}


Question 10 : Que s'affiche-t-il ?

```python
liste = [1, 4, 5, 2]
liste[1] = liste[2] + liste[0] + liste[1]
liste[2] = liste[1] + liste[3]
print(liste[2])
```

{{ qcm(["15", "3", "12", "5", "Autre"], [3], shuffle = True) }}


Question 11 : Que s'affiche-t-il ?

```python
def mystere(n):
    double = 2 * n

n = 5
print(double)
```

{{ qcm(["`#!python None`", "Un message d'erreur", "10", "5", "Autre"], [2], shuffle = True) }}


Question 12 : Que contient la variable `y` après exécution de ce code ?

```python
def mystere(n):
    print(2 * n)

y = mystere(3)
```

{{ qcm(["`#!python None`", "Rien, cela produit un message d'erreur", "6", "3", "Autre"], [1], shuffle = True) }}

Question 13 : Quel est le tableau t construit par les instructions suivantes ?

```python
t = [0] * 10
for i in range(10):
    t[i] = i
```

{{ qcm(["`#!python [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]`", "`#!python [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]`", "`#!python [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]`", "`#!python [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]`", "Autre"], [2], shuffle = True) }}

Question 14 : Quelle est l'instruction à ajouter pour que la valeur renvoyée par l'appel `somme([1, 2, 3])` soit 6 ?

```python
def somme(tab):
    s = 0
    for i in range(len(tab)):
        ...
    return s
```

{{ qcm(["`#!python s = s + i`", "`#!python tab[i] = tab[i] + s`", "`#!python s = s + tab[i]`", "`#!python s = tab[i]`", "Autre"], [3], shuffle = True) }}

Question 15 : Que s'affiche-t-il ?

```python
m1 = [[4, 5, 6], [7, 8, 9], [10, 11, 12]]
print(m1[1][1])
```

{{ qcm(["8", "4", "4, 4", "8, 8", "Autre"], [1], shuffle = True) }}

Question 16 : Que s'affiche-t-il ?

```python
mon_tuple = (15, 2)
mon_tuple[1] = 4
print(mon_tuple[1])
```

{{ qcm(["15", "2", "4", "Autre"], [4], shuffle = True) }}

Question 17 : Que s'affiche-t-il ?

```python
ma_liste = [2, 4, 8, 16]
for n in ma_liste:
    reponse = n
print(reponse)
```

{{ qcm(["16", "8", "[2, 4, 8, 16]", "Autre"], [1], shuffle = True) }}

Question 18 : Quelle instruction faut-il écrire à la place de `...` pour qu'il s'affiche

```pycon
10
20
30
```

Code à compléter :

```python
ma_liste = [10, 20, 30]
for i in ... :
    print(ma_liste[i])
```

{{ qcm(["`#!python ma_liste`", "`#!python range(ma_liste)`", "`#!python range(len(ma_liste))`", "`#!python range(len(ma_liste) - 1)`", "Autre"], [3], shuffle = True) }}

Question 19 : Que vaut `#!python [2 + n for n in range(1, 6)]` ?

{{ qcm(["`#!python [3, 4, 5, 6, 7, 8]`", "`#!python [3, 4, 5, 6, 7]`", "`#!python [2, 3, 4, 5, 6]`", "Autre"], [2], shuffle = True) }}

Question 20 : Que s'affiche-t-il ?

```python
ma_liste = [-5, 2, 4, -8, 42, 8]
print([n for n in ma_liste if n < 0])
```

{{ qcm(["`#!python [-5, -8]`", "`#!python [True, False, False, True, False, False ]`", "Autre"], [1], shuffle = True) }}