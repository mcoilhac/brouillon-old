## Annotation de code sur pseudo-code


```python title="Algorithme de dichotomie"
Fonction dichotomie(debut, fin, precision) :                   # (1)
    Tant que fin - debut > precision :
        milieu = (debut + fin) / 2
        Si f(milieu) × f(fin) est strictement positif :        # (2)
            fin = milieu
        Sinon si f(milieu) × f(fin) est strictement négatif :  # (3)                                   
            debut = milieu
        Sinon                                                  
            Renvoyer (milieu, milieu)                      
    Renvoyer (debut, fin)
```

1. La fonction prend trois paramètres : le début et la fin de la zone de recherche et la précision ;
2. `milieu` a dépassé la solution ;
3. `milieu` n'a pas dépassé la solution.


??? info "Les classes `Carte` et `Paquet`"

	Les classes `Carte` et `Paquet` sont définies ainsi :
	
	```mermaid
	classDiagram
    direction RL
    Paquet o-- Carte
    class Paquet{
        list contenu
        __init__()
        cart_pos()
        taille()
        ajoute_carte()
    } 
    class Carte {
        __init__()
        int hauteur
        couleur_carte()
        hauteur_carte()
        int couleur   
    }
    ```

	* **Classe Carte :**

	| méthode | explication |
	|:------------|:--------------|
	| __init__ | prend un entier `h` et un entier `c` et crée la carte correspondante |
	| couleur_carte | renvoie la couleur de la carte |
	| hauteur_carte | renvoie la hauteur de la carte |

	* **Classe Paquet :**

	| méthode | explication |
	|:------------|:--------------|
	| __init__ | crée un paquet vide |
	| carte_pos | prend un entier `n` et renvoie la carte à l'indice `n` |
	| taille | renvoie le nombre de cartes |


    Dans la classe `Carte`, les hauteurs et les couleurs sont repérées par les codes suivants :
	
	```python
    HAUTEURS = {
		1: 'As', 7: '7', 8: '8', 9: '9', 10: '10',
		11: 'Valet', 12: 'Dame', 13: 'Roi'
		}

    COULEURS = [None, 'pique', 'cœur', 'carreau', 'trèfle']
	```
	
	


