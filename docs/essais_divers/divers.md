---
author: Mireille Coilhac
title: Essais divers
---

## Exercice 1 : 

Écrire une fonction `maximum` :

- prenant en paramètre une liste **non vide** de nombres : `nombres`
- renvoyant le plus grand élément de cette liste.

Chacun des nombres utilisés est de type `int` ou `float`.


???+ example "Exemples"

    ```pycon
    >>> maximum([98, 12, 104, 23, 131, 9])
    131
    >>> maximum([-27, 24, -3, 15])
    24
    ```



    Compléter ci-dessous 

    {{ IDE('scripts/maximum') }}

```python hl_lines="1 3-5"
def maximum(nombres):
    maxi = nombres[0]   # rajouter cette ligne
    for x in nombres:
        if x > maxi:
            maxi = x
    return maxi
```

## Latex

Pour $409$ on fait donc :

$$\begin{align*} 
\text{somme_chiffres}(409) &=  9 + \text{somme_chiffres}(40) \\ 
 &=  9 + 0 + \text{somme_chiffres}(4) \\
 &= 9 + 0 + 4\\
 &=13
\end{align*}$$

Pour $409$ on fait donc :


$\begin{align*} 
\text{somme_chiffres}(409) &=  9 + \text{somme_chiffres}(40) \\ 
 &=  9 + 0 + \text{somme_chiffres}(4) \\
 &= 9 + 0 + 4\\
 &=13
\end{align*}$



