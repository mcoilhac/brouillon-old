---
author: Mireille Coilhac
title: essai gif Gilles Lassus
---


<gif-player src="https://glassus.github.io/terminale_nsi/T2_Programmation/2.2_Recursivite/data/arbre.gif" speed="1" play></gif-player>

Essai 1

<gif-player src="https://mcoilhac.forge.aeif.fr/brouillon/docs/essai_gif/images/2piles1file.gif" speed="1" play></gif-player>

Essai 2 sans docs

<gif-player src="https://mcoilhac.forge.aeif.fr/brouillon/essai_gif/images/2piles1file.gif" speed="1" play></gif-player>



https://mcoilhac.forge.aeif.fr/brouillon/docs/essai_gif/images/2piles1file.gif




[gif Gilles](https://glassus.github.io/terminale_nsi/T1_Structures_de_donnees/1.1_Listes_Piles_Files/data/2piles1file.gif){ .md-button target="_blank" rel="noopener" }


<div class="centre">
<iframe 
src="https://glassus.github.io/terminale_nsi/T1_Structures_de_donnees/1.1_Listes_Piles_Files/data/2piles1file.gif" 
width="640" height="360" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


![circuit gif](images/circuit.gif){ width=20% }

Par défaut l'image est placée à gauche.


![gif naif](images/gif_naive.gif){ width=90% }


!!! info "naif"

    ![gif naif](images/gif_naive.gif){ width=90% }

