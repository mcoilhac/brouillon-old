---
author: Mireille Coilhac
title: Redirection
---

!!! danger "Redirection après migration"

    Pour rediriger un site créé avec ce tutoriel (et le site modèle proposé) sur la forge AEIF vers ce même site une fois migré sur la forge Education Nationale, il suffit dans le dossier `y_theme_customizations`m d'ouvrir le fichier `main.html`. Au début du fichier, il faut ajouter la dernière ligne du bloc ci-dessous : 

    ````html
    {% raw %}
    {% extends "base.html" %}

    {% block content %}
    {{super()}}
    <script src="{{ base_url }}/pyodide-mkdocs/ide.js"></script> 
    {% endblock %}

    {% block extrahead %}
    <meta http-equiv="refresh" content="0;url={{ [config.site_url,page.url] | join('/') | replace('aeif','apps.education') }}">
    {% endraw %}
    ````

    ````html title="Ligne à recopier"
    {% raw %}
    <meta http-equiv="refresh" content="0;url={{ [config.site_url,page.url] | join('/') | replace('aeif','apps.education') }}">
    {% endraw %}
    ````  
